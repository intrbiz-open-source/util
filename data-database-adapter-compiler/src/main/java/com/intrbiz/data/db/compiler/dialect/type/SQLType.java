package com.intrbiz.data.db.compiler.dialect.type;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public interface SQLType
{
    String getSQLType();
    
    default Set<String> getSQLTypes() {
        Set<String> set = new HashSet<>();
        set.add(this.getSQLType());
        set.addAll(this.getSQLTypeAliases());
        return set;
    }
    
    default Set<String> getSQLTypeAliases() {
        return Collections.emptySet();
    }
    
    Class<?>[] getJavaTypes();
    
    default Class<?> getDefaultJavaType() {
        return this.getJavaTypes()[0];
    }
    
    boolean isCompatibleWith(Class<?> type);
    
    //
    
    void addImports(Set<String> imports);
    
    String setBinding(int idx, String value);
    
    String getBinding(int idx);
}
