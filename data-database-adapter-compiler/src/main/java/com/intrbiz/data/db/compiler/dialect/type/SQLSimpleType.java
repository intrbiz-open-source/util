package com.intrbiz.data.db.compiler.dialect.type;

import java.util.HashSet;
import java.util.Set;

public class SQLSimpleType implements SQLType
{
    private final String sqlType;
    
    private Set<String> sqlTypeAliases = new HashSet<String>();
    
    private final Class<?>[] javaTypes;
    
    private final String jdbcAccessor;

    public SQLSimpleType(String sqlType, String jdbcAccessor, Class<?>... javaTypes)
    {
        this.sqlType = sqlType;
        this.jdbcAccessor = jdbcAccessor;
        this.javaTypes = javaTypes;
    }

    @Override
    public String getSQLType()
    {
        return this.sqlType;
    }
    
    public SQLSimpleType withSQLTypeAliases(String... aliases) {
        for (String alias : aliases) {
            this.sqlTypeAliases.add(alias);
        }
        return this;
    }
    
    @Override
    public Set<String> getSQLTypeAliases() {
        return this.sqlTypeAliases;
    }

    @Override
    public Class<?>[] getJavaTypes()
    {
        return this.javaTypes;
    }
    
    public boolean isCompatibleWith(Class<?> type)
    {
        for (Class<?> c : this.getJavaTypes())
        {
            if (type.isAssignableFrom(c))
                return true;
        }
        return false;
    }
    
    public void addImports(Set<String> imports)
    {
    }
    
    public String setBinding(int idx, String value)
    {
        return "stmt.set" + this.jdbcAccessor + "(" + idx + ", " + value + ")";
    }
    
    public String getBinding(int idx)
    {
        return "rs.get" + this.jdbcAccessor + "(" + idx + ")";
    }
}
